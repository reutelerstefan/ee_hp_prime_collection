#pragma mode( separator(.,;) integer(h32) )

//MENUETREE={bar1, bar2, ...}
//bar={button1, button2, ..,}
//button={text, cmd, list} //2 options: (cmd <> "" AND list = {}) OR (cmd = "" AND list not empty)
//list={item1, item2, ...}
//item={text, cmd} 

LOCAL MENUTREE:={
{
 {"i", "",
  {}
 },
 {"re", "re(z)",
  {}
 }, 
 {"im", "im(z)",
  {}
 },
 {"abs", "abs(z)",
  {}
 }, 
 {"arg", "arg(z)",
  {}
 },
 {"conj", "conj(z)",
  {}
 },
 {"sign", "sign(z)",
  {}
 },
 {"C..","",
   {{"InfoC(z)", "InfoC(z)"}, {"ListC(z)", "ListC(z)"}, 
    {"Arg14(z)", "Arg14(z)"}, {"Arg2(z)", "Arg2(z)"}, {"Arg3(z)", "Arg3(z)"},
    {"evalc(z)", "evalc(z)"}, {"mult_c_conjugate(z)", "mult_c_conjugate(z)"}}
 }
},
{
 {"Tran..","",
  {{"left()", "left()"}, {"right()", "right()"},
   {"numer()", "numer()"}, {"denom()", "denom()"},
   {"subst(x/(4*x),x=3)", "subst(x/(4*x),x=3)"}, {"assume(x>0)", "assume(x>0)"}, {"purge(x,y)", "purge(x,y)"}}
 },
 {"Sol..","",
  {{"solve({h1,h2},{u,j})(1)▶u", "solve({h1,h2},{u,j})(1)▶u"}, {"zeros({h1,h2},{u,j})(1)▶u", "zeros({h1,h2},{u,j})(1)▶u"}}
 },
 {"restart","restart",
  {}
 }
},
{
 {"f..","",
   {{"w8f(w,f)", "w8f(w,f)▶h"}, {"f(w)", "w2f(w)▶f"}, {"w(f)", "f2w(f)▶w"}, 
    {"t8f(t,f)", "t8f(t,f)▶h"}, {"f(t)", "t2f(t)▶f"}, {"t(f)", "f2t(f)▶t"}}
 },
 {"Z..","",
  {{"z8wc(z,w*c)", "z8wc(z,w*c)▶h"}, {"wc(z)", "z2wc(z)▶wc"}, {"z(w*c)", "wc2z(w*c)▶z"},
   {"z8wl(z,w*l)", "z8wl(z,w*l)▶h"}, {"wl(z)", "z2wl(z)▶wl"}, {"z(w*l)", "wl2z(w*l)▶z"}}
 },
 {"Y..","",
  {{"y8wc(y,w*c)", "y8wc(y,w*c)▶h"}, {"wc(y)", "y2wc(y)▶wc"}, {"y(w*c)", "wc2y(w*c)▶y"},
   {"y8wl(y,w*l)", "y8wl(y,w*l)▶h"}, {"wl(y)", "y2wl(y)▶wl"}, {"y(w*l)", "wl2y(w*l)▶y"}}
 },
 {"NW..","",
  {{"Z=Z1+Z2", "zs8(z,z1,z2)▶h"}, {"Z1+Z2", "zs2(z1,z2)▶z"}, 
   {"Z=Z1||Z2", "zp8(z,z1,z2)▶h"}, {"Z1||Z2", "zp2(z1,z2)▶z"},
   {"Y=Y1+Y2", "ys8(y,y1,y2)▶h"}, {"Y1+Y2", "ys2(y1,y2)▶y"},
   {"Y=Y1||Y2", "yp8(y,y1,y2)▶h"}, {"Y1||Y2", "yp2(y1,y2)▶y"}}
 },
 {"S..","",
  {{"s8u8i(s,u,j)", "s8u8i(s,u,j)▶h"}, 
   {"s(u,j)", "u8i2s(u,j)▶s"}, {"u(s,j)", "s8i2u(s,j)▶u"}, {"i(s,u)", "s8u2i(s,u)▶j"}}
 },
 {"S↔P..","",
  {{"RpL(w,z)", "rpl(w,z)"}, {"Lp(w,z)", "lp(w,z)"},
   {"RpC(w,z)", "rpc(w,z)"}, {"Cp(w,z)", "cp(w,z)"},
   {"RsL(w,y)", "rsl(w,y)"}, {"Ls(w,y)", "ls(w,y)"},
   {"RsC(w,y)", "rsc(w,y)"}, {"Cs(w,y)", "cs(w,y)"}}
 }
}
};

EXPORT CheckMenuTree()
BEGIN
 LOCAL errorMsgPre:="E: CheckMenuTree(): ";
 LOCAL error:=0;

 IF TYPE(MENUTREE) ≠ 6 THEN
  MSGBOX(errorMsgPre + "MENUTREE not a List");
  error:=1;
 END;
 IF SIZE(MENUTREE) = 0 THEN
  MSGBOX(errorMsgPre +  "MENUTREE without bar");
  error:=1;
 END;
 
 LOCAL bar;
 FOR bar FROM 1 TO SIZE(MENUTREE) DO
  LOCAL errorMsgPreBar:=errorMsgPre + "bar: " + bar + ": ";

  IF TYPE(MENUTREE(bar)) ≠ 6 THEN
   MSGBOX(errorMsgPreBar + "not a List");
   error:=1;
  END;
  IF SIZE(MENUTREE(bar)) = 0 THEN
   MSGBOX(errorMsgPreBar + "without buttons");
   error:=1;
  END;
 
  LOCAL button;
  FOR button FROM 1 TO SIZE(MENUTREE(bar)) DO
   LOCAL errorMsgPreButton:=errorMsgPreBar + "button: " + button + ": ";

   IF TYPE(MENUTREE(bar, button)) ≠ 6 THEN
    MSGBOX(errorMsgPreButton + "not a List");
    error:=1;
   END;
   IF SIZE(MENUTREE(bar, button)) ≠ 3 THEN
    MSGBOX(errorMsgPreButton + "not of SIZE 3");
    error:=1;
   END;
   IF MENUTREE(bar, button, 1) = "" THEN
    MSGBOX(errorMsgPreButton + "text empty");
    error:=1;
   END;
   IF TYPE(MENUTREE(bar, button, 3)) ≠ 6 THEN
    MSGBOX(errorMsgPreButton + "list not a List");
    error:=1;
   END;
   IF (MENUTREE(bar, button, 2) = "" AND SIZE(MENUTREE(bar, button, 3)) = 0) THEN
    MSGBOX(errorMsgPreButton + "no command and list empty");
    error:=1;
   END;
   IF (MENUTREE(bar, button, 2) ≠ "" AND SIZE(MENUTREE(bar, button, 3)) > 0) THEN
    MSGBOX(errorMsgPreButton + "command and non-empty list provided");
    error:=1;
   END;
   
   LOCAL item;
   FOR item FROM 1 TO SIZE(MENUTREE(bar, button, 3)) DO
    LOCAL errorMsgPreItem:=errorMsgPreButton + "item: " + item + ": ";
    
    IF TYPE(MENUTREE(bar, button, 3, item)) ≠ 6 THEN
     MSGBOX(errorMsgPreItem + "not a list");
     error:=1;
    END;
    IF SIZE(MENUTREE(bar, button, 3, item)) ≠ 2 THEN
     MSGBOX(errorMsgPreButton + "not of SIZE 2");
     error:=1;
    END;
    IF MENUTREE(bar, button, 3, item, 1) = "" THEN
     MSGBOX(errorMsgPreButton + "text empty");
     error:=1;
    END;

    IF MENUTREE(bar, button, 3, item, 2) = "" THEN
     MSGBOX(errorMsgPreButton + "no command");
     error:=1;
    END;
   END;//item 
  END;//button 
 END;//bar 
 
 RETURN error;
END;//CheckMenuTree()


//up
EXPORT GetUpMenuBar(currMenuBar)
BEGIN
 IF currMenuBar = 1 THEN
  RETURN SIZE(MENUTREE);
 ELSE
  RETURN currMenuBar-1;
 END;
END;

//down
EXPORT GetDwMenuBar(currMenuBar)
BEGIN

//MSGBOX("DW currMenuBar: "+currMenuBar);

 IF currMenuBar = SIZE(MENUTREE) THEN
//MSGBOX("DW currMenuBar: "+1);

  RETURN 1;
 ELSE

//MSGBOX("DW currMenuBar+1r: "+(currMenuBar+1));

  RETURN currMenuBar+1;
 END;
END;

//first MenuBarWin=1

//last
EXPORT GetLstMenuBarWin(bar)
BEGIN
 RETURN 1+FLOOR((SIZE(MENUTREE(bar))-1)/6);
END;

//next
EXPORT GetNextNxtMenuBarWin(bar, currMenuBarWin)
BEGIN
 IF currMenuBarWin=GetLstMenuBarWin(bar) THEN
  RETURN 1;
 ELSE
  RETURN currMenuBarWin+1;
 END;
END;

//previous
EXPORT GetNextPreMenuBarWin(bar, currMenuBarWin)
BEGIN
 IF currMenuBarWin=1 THEN
  RETURN GetLstMenuBarWin(bar);
 ELSE
  RETURN currMenuBarWin-1;
 END;
END;

///////////////////////////////////////////////////////////

EXPORT MENULEAF:={1,1,0};//bar, button, item

EXPORT CheckMenuLeaf()
BEGIN
 LOCAL errorMsgPre:="E: CheckMenuLeaf(): ";
 LOCAL error:=0;

 IF TYPE(MENULEAF) ≠ 6 THEN
  MSGBOX(errorMsgPre + "MENULEAF not a list");
  error:=1;
 END; 
 IF SIZE(MENULEAF) ≠ 3 THEN
   MSGBOX(errorMsgPre +  "MENULEAF not of SIZE 3");
  error:=1;
 END;

 IF TYPE(MENULEAF(1)) ≠ 0 THEN
   MSGBOX(errorMsgPre +  "bar not a Real");
  error:=1;
 END; 
 IF TYPE(MENULEAF(2)) ≠ 0 THEN
   MSGBOX(errorMsgPre +  "button not a Real");
  error:=1;
 END;
 IF TYPE(MENULEAF(3)) ≠ 0 THEN
   MSGBOX(errorMsgPre +  "item not a Real");
  error:=1;
 END;

 LOCAL bar:=MENULEAF(1);
 IF (bar < 1 OR SIZE(MENUTREE) < bar) THEN
  MSGBOX(errorMsgPre +  "bar out of range");
  error:=1
 END;

 LOCAL button:=MENULEAF(2);
 IF (button < 1 OR SIZE(MENUTREE(bar)) < button) THEN
  MSGBOX(errorMsgPre +  "button out of range");
  error:=1
 END; 
 
 IF SIZE(MENUTREE(bar, button, 3)) = 0 THEN
  IF MENULEAF(3) ≠ 0 THEN
   MSGBOX(errorMsgPre +  "button in range of MENUTREE");
   error:=1;
  END;
 ELSE
  LOCAL item:=MENULEAF(3);
  IF (item < 1 OR SIZE(MENUTREE(bar, button, 3)) < item) THEN
   MSGBOX(errorMsgPre +  "item in range of MENUTREE");
   error:=1
  END;   
 END;
 
 RETURN error;
END;

///////////////////////////////////////////////////////////

EXPORT MENUBARWIN:=1;

//first
EXPORT GetFstButton(menuBar, menuBarWin)
BEGIN
 LOCAL fst:=6*menuBarWin-5;
 IF SIZE(MENUTREE(menuBar)) < fst THEN 
  MSGBOX("E: GetFstButton(): menuBarWin not in range of MENUTREE");
  RETURN 0; 
 END;
 
 RETURN fst;
END;

//last
EXPORT GetLstButton(menuBar, menuBarWin)
BEGIN
 LOCAL lst:=6*menuBarWin;
 LOCAL sz:=SIZE(MENUTREE(menuBar));
 IF sz < lst-5 THEN 
  MSGBOX("E: GetLstButton(): menuBarWin not in range of MENUTREE");
  RETURN 0; 
 END;
 
 RETURN MIN(lst, sz);
END;

///////////////////////////////////////////////////////////

EXPORT GetMenu()
BEGIN
 IF (CheckMenuTree() OR CheckMenuLeaf()) THEN RETURN ""; END;

 REPEAT
  LOCAL bar:=MENULEAF(1);
  LOCAL fstBarWinButton:=GetFstButton(bar, MENUBARWIN);
  LOCAL lstBarWinButton:=GetLstButton(bar, MENUBARWIN);

//MSGBOX("fstBarWinButton: "+fstBarWinButton);
//MSGBOX("lstBarWinButton: "+lstBarWinButton);


  LOCAL barWinTxt:={}, b, bw=1;
  FOR b FROM fstBarWinButton TO lstBarWinButton DO
   barWinTxt(bw):=MENUTREE(bar, b, 1);
   bw:=bw+1;
  END;

//MSGBOX("barWinTxt: "+barWinTxt);

  DRAWMENU(barWinTxt);
  
  LOCAL waitRet:=WAIT(−1);

//MSGBOX("waitRet: "+waitRet);

  IF TYPE(waitRet)==6 THEN
   //Mouse event
   IF B→R(waitRet(1))==3 THEN
    // Pick coordinates
    LOCAL mouseX:=B→R(waitRet(2));
    LOCAL mouseY:=B→R(waitRet(3));
    
    // Check if in menu area:
    IF mouseY>220 THEN
     LOCAL buttonWin=0;
     CASE
      IF 0≤mouseX≤51 THEN buttonWin:=1; END;
      IF 53≤mouseX≤104 THEN buttonWin:=2; END;
      IF 106≤mouseX≤157 THEN buttonWin:=3; END;
      IF 159≤mouseX≤210 THEN buttonWin:=4; END;
      IF 212≤mouseX≤263 THEN buttonWin:=5; END;
      IF 265≤mouseX≤319 THEN buttonWin:=6; END;
      DEFAULT
     END;//case mouseX
    
     // Check for short menu:
     IF (1 ≤ buttonWin AND buttonWin ≤ SIZE(barWinTxt)) THEN
      LOCAL button:=fstBarWinButton + buttonWin - 1;
      IF (button ≠ MENULEAF(2)) THEN
       MENULEAF(2):=button;
       MENULEAF(3):=1;
      END;

      LOCAL cmd:=GetMenuCmd();
      IF cmd ≠ "" THEN RETURN cmd; END     
     END;
    END;//my>220
   END;//B→R(waitRet(1))==3
  ELSE
   //Button event
   CASE
    //Previous(7)/next(8) bar window
    IF waitRet==7 THEN 
     MENUBARWIN:=GetNextPreMenuBarWin(MENULEAF(1), MENUBARWIN);
     MENULEAF(3):=1; 
    END;
    IF waitRet==8 THEN 

//MSGBOX("MENUBARWIN bef: "+MENUBARWIN);

     MENUBARWIN:=GetNextNxtMenuBarWin(MENULEAF(1), MENUBARWIN); 

//MSGBOX("MENUBARWIN after: "+MENUBARWIN);

     MENULEAF(3):=1;
    END;

    //Up(2)/down(12) bar
    IF waitRet==2 THEN 

//MSGBOX("MENULEAF 1: "+MENULEAF);

     MENULEAF:={GetUpMenuBar(MENULEAF(1)), 1, 1};
     MENUBARWIN:=1;

//MSGBOX("MENULEAF 2: "+MENULEAF);

    END;
    IF waitRet==12 THEN 

//MSGBOX("MENULEAF 1: "+MENULEAF);

     MENULEAF:={GetDwMenuBar(MENULEAF(1)), 1, 1};
     MENUBARWIN:=1;

//MSGBOX("MENULEAF 2: "+MENULEAF);

    END;

    //Esc or On to exit:
    IF (waitRet==4 OR waitRet==46) THEN RETURN ""; END;
    DEFAULT
   END;//case w  
  END;//TYPE(w)
 UNTIL 0;
END;//GetMenu()

//////////////////////////////////////////

//getCmd
EXPORT GetMenuCmd()
BEGIN
 LOCAL bar:=MENULEAF(1), button:=MENULEAF(2), item:=MENULEAF(3);
 LOCAL listSize:=SIZE(MENUTREE(bar, button, 3));
 


 IF (listSize = 0) THEN
  MENULEAF(3):=0; 
  RETURN MENUTREE(bar, button, 2); 
 END;

//MSGBOX("GetMenuCmd");

 LOCAL itemTxt:={}, itemCmd:={}, i;
 FOR i FROM 1 TO listSize DO
  itemTxt(i):=MENUTREE(bar, button, 3, i, 1);
  itemCmd(i):=MENUTREE(bar, button, 3, i, 2);
 END;
   
 LOCAL choice:=item;
 LOCAL chooseRet:=CHOOSE(choice, MENUTREE(bar, button, 1), itemTxt);

 IF (chooseRet=0 OR choice < 1 OR listSize < choice) THEN
  RETURN "";
 ELSE 
  RETURN itemCmd(choice);
 END;  
END;//GetMenuCmd()