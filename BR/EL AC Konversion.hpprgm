EXPORT Rp2RsL()
BEGIN
    // Eingabe der Variablen von außen
    LOCAL Rp, Lp, freq;
    INPUT({Rp,Lp,freq} ,"Rp to Rs:",{"Rp = ","Lp = ","f = "});

    // Ausgabe der eingegebenen Variablen
    PRINT("Rp: " + STRING(Rp,4,6) + " Ω");
    PRINT("Lp: " + STRING(Lp,4,6) + " H");
    PRINT("f: " + STRING(freq,4,6) + " Hz");
    
    // Berechnung von Rs
    LOCAL Rs := 
    ((2*π*freq)^2*Rp*Lp^2)/(Rp^2+(2*π*freq)^2*Lp^2);

    // Ausgabe der berechneten Rs
    PRINT("Konversion - Widerstand NEU seriel [Rs]:");
    PRINT("Rs: " + STRING(Rs,4,6) + " Ω");
    RETURN Rs;
    // TEST: 100, 0.000001, 10000000 = 28.3 Ohm
END;

EXPORT Lp2Ls()
BEGIN
    // Eingabe der Variablen von außen
    LOCAL Rp, Lp, freq;
    INPUT({Rp,Lp,freq} ,"Lp to Ls:",{"Rp= ","Lp= ","f= "});

    // Ausgabe der eingegebenen Variablen
    PRINT("Rp: " + STRING(Rp,4,6) + " Ω");
    PRINT("Lp: " + STRING(Lp,4,6) + " H");
    PRINT("f: " + STRING(freq,4,6) + " Hz");
    
    // Berechnung von Ls
    LOCAL Ls := 
    (Rp^2*Lp)/(Rp^2+(2*π*freq)^2*Lp^2);

    // Ausgabe der berechneten Ls
    PRINT("Konversion - Induktivität NEU seriel [Ls]:");
    PRINT("Ls: " + STRING(Ls,4,6) + " H");
    RETURN Ls;
    // TEST: 100, 0.000001, 10000000 = 717nH
END;

EXPORT Rs2RpL()
BEGIN
    // Eingabe der Variablen von außen
    LOCAL Rs, Ls, freq;
    INPUT({Rs,Ls,freq} ,"Rs to Rp:",{"Rs= ","Ls= ","f= "});

    // Ausgabe der eingegebenen Variablen
    PRINT("Rs: " + STRING(Rs,4,6) + " Ω");
    PRINT("Ls: " + STRING(Ls,4,6) + " H");
    PRINT("f: " + STRING(freq,4,6) + " Hz");
    
    // Berechnung von Rp
    LOCAL Rp := 
    (Rs^2+(2*π*freq)^2*Ls^2)/(Rs);

    // Ausgabe der berechneten Rp
    PRINT("Konversion - Widerstand NEU parallel [Rp]:");
    PRINT("Rp: " + STRING(Rp,4,6) + " Ω");
    RETURN Rp;
    // TEST: 28.3, 0.000000717, 10000000 = 100 Ohm
END;

EXPORT Ls2Lp()
BEGIN
    // Eingabe der Variablen von außen
    LOCAL Rs, Ls, freq;
    INPUT({Rs,Ls,freq} ,"Ls to Lp:",{"Rs= ","Ls= ","f= "});

    // Ausgabe der eingegebenen Variablen
    PRINT("Rs: " + STRING(Rs,4,6) + " Ω");
    PRINT("Ls: " + STRING(Ls,4,6) + " H");
    PRINT("f: " + STRING(freq,4,6) + " Hz");
    
    // Berechnung von Lp
    LOCAL Lp := 
    (Rs^2+(2*π*freq)^2*Ls^2)/((2*π*freq)^2*Ls);

    // Ausgabe der berechneten Lp
    PRINT("Konversion - Induktivität NEU parallel [Lp]:");
    PRINT("Lp: " + STRING(Lp,4,6) + " H");
    RETURN Lp;
    // TEST: 28.3, 0.000000717, 10000000 = 1µH
END;

EXPORT Rp2RsC()
BEGIN
    // Eingabe der Variablen von außen
    LOCAL Rp, Cp, freq;
    INPUT({Rp,Cp,freq} ,"Rp to Rs:",{"Rp= ","Cp= ","f= "});

    // Ausgabe der eingegebenen Variablen
    PRINT("Rp: " + STRING(Rp,4,6) + " Ω");
    PRINT("Cp: " + STRING(Cp,4,6) + " F");
    PRINT("f: " + STRING(freq,4,6) + " Hz");
    
    // Berechnung von Rs
    LOCAL Rs := 
    (Rp)/(1+Rp^2*(2*π*freq)^2*Cp^2);

    // Ausgabe der berechneten Rs
    PRINT("Konversion - Widerstand NEU seriel [Rs]:");
    PRINT("Rs: " + STRING(Rs,4,6) + " Ω");
    RETURN Rs;
    // TEST: 100, 0.0000000001, 10000000 = 71.7 Ohm
END;

EXPORT Cp2Cs()
BEGIN
    // Eingabe der Variablen von außen
    LOCAL Rp, Cp, freq;
    INPUT({Rp,Cp,freq} ,"Cp to Cs:",{"Rp= ","Cp= ","f= "});

    // Ausgabe der eingegebenen Variablen
    PRINT("Rp: " + STRING(Rp,4,6) + " Ω");
    PRINT("Cp: " + STRING(Cp,4,6) + " F");
    PRINT("f: " + STRING(freq,4,6) + " Hz");
    
    // Berechnung von Cs
    LOCAL Cs := 
    (1+Rp^2*(2*π*freq)^2*Cp^2)/(Rp^2*(2*π*freq)^2*Cp);

    // Ausgabe der berechneten Cs
    PRINT("Konversion - Kapazität NEU seriel [Cs]:");
    PRINT("Cs: " + STRING(Cs,4,6) + " F");

    RETURN Cs;
    // TEST: 100, 0.0000000001, 10000000 = 0.000000000353 F
END;

EXPORT Rs2RpC()
BEGIN
    // Eingabe der Variablen von außen
    LOCAL Rs, Cs, freq;
    INPUT({Rs,Cs,freq} ,"Rs to Rp:",{"Rs= ","Cs= ","f= "});

    // Ausgabe der eingegebenen Variablen
    PRINT("Rs: " + STRING(Rs,4,6) + " Ω");
    PRINT("Cs: " + STRING(Cs,4,6) + " F");
    PRINT("f: " + STRING(freq,4,6) + " Hz");
    
    // Berechnung von Rp
    LOCAL Rp := 
    (Rs+(1/(Rs*(2*π*freq)^2*Cs^2)));

    // Ausgabe der berechneten Rp
    PRINT("Konversion - Widerstand NEU parallel [Rp]:");
    PRINT("Rp: " + STRING(Rp,4,6) + " Ω");
    RETURN Rp;
    // TEST: 71.7, 0.000000717, 10000000 = 100 Ohm
END;

EXPORT Cs2Cp()
BEGIN
    // Eingabe der Variablen von außen
    LOCAL Rs, Cs, freq;
    INPUT({Rs,Cs,freq} ,"Cs to Cp:",{"Rs= ","Cs= ","f= "});

    // Ausgabe der eingegebenen Variablen
    PRINT("Rs: " + STRING(Rs,4,6) + " Ω");
    PRINT("Cs: " + STRING(Cs,4,6) + " F");
    PRINT("f: " + STRING(freq,4,6) + " Hz");
    
    // Berechnung von Cp
    LOCAL Cp := 
    ((Cs/(1+Rs^2*(2*π*freq)^2*Cs^2)));

    // Ausgabe der berechneten Cp
    PRINT("Konversion - Kapazität NEU parallel [Cp]:");
    PRINT("Cp: " + STRING(Cp,4,6) + " F");
    RETURN Cp;
    // TEST: 71.7, 0.000000000353, 10000000 = 0.0000000001F
END;

EXPORT D2Y()
BEGIN
    // Eingabe der Variablen von außen
    PRINT();
    LOCAL Z12:=1+1*i,
          Z23:=1+1*i,
          Z31:=1+1*i;

    INPUT({Z12,Z23,Z31} ,"Dreick to Stern:",{"Z12= ","Z23= ","Z31= "});

    // Ausgabe der eingegebenen Variablen
    PRINT("Konversion - Widerstände ALT Dreieck [Z1-Z3]:");
    PRINT("Z12: " + STRING(Z12,4,6) + " Ω");
    PRINT("Z23: " + STRING(Z23,4,6) + " Ω");
    PRINT("Z31: " + STRING(Z31,4,6) + " Ω");
    
    // Berechnung von Z Ersatz Stern
    LOCAL Z1 := 
    ((Z12*Z31)/(Z12+Z23+Z31));

    LOCAL Z2 := 
    ((Z12*Z23)/(Z12+Z23+Z31));

    LOCAL Z3 := 
    ((Z23*Z31)/(Z12+Z23+Z31));

    // Ausgabe der berechneten Cp
    PRINT("");
    PRINT("Konversion - Widerstand NEU Stern [Z1-Z3]:");
    PRINT("Z1: " + STRING(Z1,4,6) + " Ω");
    PRINT("Z2: " + STRING(Z2,4,6) + " Ω");
    PRINT("Z3: " + STRING(Z3,4,6) + " Ω");

    RETURN ({Z1,Z2,Z3});

END;

EXPORT Y2D()
BEGIN
    // Eingabe der Variablen von außen
    PRINT();
    LOCAL Z1:=1+1*i,
          Z2:=1+1*i,
          Z3:=1+1*i;

    INPUT({Z1,Z2,Z3} ,"Dreick to Stern:",{"Z1= ","Z2= ","Z3= "});

    // Ausgabe der eingegebenen Variablen
    PRINT("Konversion - Widerstände ALT Dreieck [Z1-Z3]:");
    PRINT("Z1: " + STRING(Z1,4,6) + " Ω");
    PRINT("Z2: " + STRING(Z2,4,6) + " Ω");
    PRINT("Z3: " + STRING(Z3,4,6) + " Ω");
    
    // Berechnung von Z Ersatz Stern
    LOCAL Z12 := 
    (Z1+Z2+((Z1*Z2)/Z3));

    LOCAL Z23 := 
    (Z2+Z3+((Z2*Z3)/Z1));

    LOCAL Z31 := 
    (Z1+Z3+((Z1*Z3)/Z2));

    // Ausgabe der berechneten Cp
    PRINT("");
    PRINT("Konversion - Widerstand NEU Stern [Z1-Z3]:");
    PRINT("Z12: " + STRING(Z12,4,6) + " Ω");
    PRINT("Z23: " + STRING(Z23,4,6) + " Ω");
    PRINT("Z31: " + STRING(Z31,4,6) + " Ω");

    RETURN ({Z12,Z23,Z31});

END;